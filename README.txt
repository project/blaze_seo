DESCRIPTION
===========
Provides a snipppet code for alalisys SEO

SITE BUILDERS
=============


CREATE TABLE dsx_node_blazeseo_analyzer (
`sid` INT unsigned NOT NULL auto_increment, 
`nid` INT NOT NULL DEFAULT 1 COMMENT 'Related nid', 
`optimized` FLOAT NOT NULL DEFAULT 0, 
`keyword_density` FLOAT NOT NULL DEFAULT 0,
`cloud_words` LONGTEXT NULL DEFAULT NULL,
PRIMARY KEY (`sid`) 
) ENGINE = InnoDB DEFAULT CHARACTER SET utf8;
