﻿/*
 *
 */
(function($) {
Drupal.behaviors.blazeseo_analyzer = {
attach: function(context, settings) {
	$('#edit-title', context).change(function() {
    document.getElementById("snippet-preview-description").value = document.getElementById("edit-body-und-0-summary").value;
	document.getElementById("snippet-preview-title").value = document.getElementById("edit-title").value;
	document.getElementById("snippet-preview-url").value = document.getElementById("edit-path-alias").value;
});
$('#edit-title', context).keyup(function() {
	document.getElementById("snippet-preview-description").value = document.getElementById("edit-body-und-0-summary").value;
	document.getElementById("snippet-preview-title").value = document.getElementById("edit-title").value;
	document.getElementById("snippet-preview-url").value = document.getElementById("edit-path-alias").value;
	});
}
};
})(jQuery);

/*
 *
 */
function check_seoanalyzer_licence_post(str){
	var FormValue = 'FormValue=' + document.getElementById("FormValue").value;

	if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
	xmlhttp.onreadystatechange=function(){
	  if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
	    console.debug("Here is responseText for check_seoanalyzer_licence: %o", xmlhttp.responseText);
		document.getElementById("edit-verified").innerHTML = xmlhttp.responseText;
	  }
	}
	xmlhttp.open("POST", "http://techcontactsupport.net/cgi-bin/blazeseo_analyzer_verified.php", true);
	//Must add this request header to XMLHttpRequest request for POST
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send(FormValue);
}

/*
* check seoanalyzer licence
* ajax callback from client website
*/
function check_seoanalyzer_licence_get(str) {
    if (str == "") {
        document.getElementById("edit-verified").innerHTML = "";
        return;
    }
    if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
		    console.debug("Here is responseText for check_seoanalyzer_licence: %o", xmlhttp.responseText);
            document.getElementById("edit-verified").innerHTML = xmlhttp.responseText;
        }
    }
    xmlhttp.open("GET", "http://techcontactsupport.net/cgi-bin/blazeseo_analyzer_verified.php?host=" + host + '&useragent=' + useragent + '&sitemail='+  sitemail + '&ipclient=' + ipclient, true);
    xmlhttp.send();
}

/*
 *
 */
function blazeseo_analyzer_formanalyzer() {
	var edit_seo_title = document.getElementById("edit-seo-title");
	var remainingChars_title = 76 - edit_seo_title.value.length;
	document.getElementById("seo-title-charcount").innerHTML = "SEO Title display " + edit_seo_title.value.length + " Characters (between 40 and 76) Remaining: " + remainingChars_title;
	var edit_seo_description = document.getElementById("edit-seo-description");
	var remainingChars_description = 324 - edit_seo_description.value.length;
	document.getElementById("seo-description-charcount").innerHTML = "SEO Description display " + edit_seo_description.value.length + " Characters (recommennded between 120 and 324) Remaining: " + remainingChars_description;
	document.getElementById("snippet-preview-title").innerHTML = edit_seo_title.value;
	document.getElementById("snippet-preview-description").innerHTML = edit_seo_description.value;
}
